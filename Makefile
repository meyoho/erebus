VERSION := $(shell git describe --always --long --dirty)

.PHONY: all test clean

mod:
	GO111MODULE=on go mod tidy
	GO111MODULE=on go mod vendor

build:
	env GOOS=linux GOARCH=amd64 GO111MODULE=on CGO_ENABLED=0  go build -mod vendor -ldflags "-w -s -X alauda.io/erebus/pkg/options.Version=${VERSION}" -v -o erebus

build.arm:
	env GOOS=linux GOARCH=arm64 GO111MODULE=on CGO_ENABLED=0  go build -mod vendor -ldflags "-w -s -X alauda.io/erebus/pkg/options.Version=${VERSION}" -v -o erebus

test:
	go test  -v -cover -coverprofile=artifacts/coverage.out ./pkg/...

show-coverage:
	go tool cover -html=artifacts/coverage.out -o /tmp/coverage.html
	open /tmp/coverage.html

fmt:
	find ./pkg -name \*.go  | xargs goimports -w

lint:
	golangci-lint run -c hack/ci/golangci.yml
	revive -config hack/ci/revive.toml -formatter friendly ./pkg/...

image:
	DOCKER_BUILDKIT=1 docker build -t erebus -f artifacts/image/Dockerfile .

push:
	docker tag erebus index.alauda.cn/alaudaorg/erebus:latest
	docker push index.alauda.cn/alaudaorg/erebus:latest

docker-compose:
	docker-compose -p ap -f artifacts/compose/docker-compose.yaml build --force-rm
	docker-compose -p ap -f artifacts/compose/docker-compose.yaml up -d

all: fmt build lint test

compose: image docker-compose

.PHONY : all
