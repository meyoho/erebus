# Erebus
This component's mainly feature is to provide a proxy for multiple kubernetes clusters.

pronunciation: `[ˈeribəs]` [dict](https://cn.bing.com/dict/search?q=Erebus&go=Search&qs=ds&form=Z9LH5)

## Usage

Just add this prefix to the kubernetes api path

`/kubernetes/<cluster-name>`

## Documents
Please read the files in the `docs/` dir.

## Feature

* multi cluster support(staticfile,furion,clusterregisty)
* kubectl support
* client-go support
* prometheus support
* feature gate


## TODO
1. tls cert support

# Links
1. [client-go-test](https://bitbucket.org/mathildetech/client-go-test/src/master/main.go)
