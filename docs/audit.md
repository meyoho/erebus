# Audit

[TOC]

Erebus support audit like kubernetes, with the same cmd args and configuration files, the full documentation can be found at [Auditing](https://kubernetes.io/docs/tasks/debug-application-cluster/audit/)。A few things to notice:



1. The `requestURI` field in the audit event will be prefixed with `/kubernetes/<cluster>`
2. If the request is provided with OIDC token, user info will be recored in the `user` field.



For simple test, there is a sample policy file in the source dir, just run 

`./erebus --audit-policy-file=artifacts/example/audit/all-metadata.yaml --audit-log-path=artifacts/audit.log --audit-log-format=json`



the json format audit event will be logged at `artifacts/audit.log` file if received requests.



