# Use ClusterRegistry

[TOC]


## Use erebus with ClusterRegistry

### Configure ClusterRegistry

First , create the crd

```bash
 kubectl apply -f https://raw.githubusercontent.com/kubernetes/cluster-registry/master/cluster-registry-crd.yaml
```



Then create example Secret and ClusterRegistry

```bash
cd $EREBUS_ROOT/artifacts/deploy/clusterregistry
kubectl apply -f *
```

The full content of the yamls are:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: mysecret
  namespace: system
type: Opaque
data:
  token: YWFhCg==
```



```yaml
kind: Cluster
apiVersion: clusterregistry.k8s.io/v1alpha1
metadata:
  name: test-cluster
  namespace: system
spec:
  authInfo:
    controller:
      kind: Secret
      name: mysecret
      namespace: kube-system
  kubernetesApiEndpoints:
    serverEndpoints:
      - clientCIDR: "0.0.0.0/0"
        serverAddress: "https://172.19.16.12:6443"

```



Then we can check the cluster we create:

```bash
kubectl get cluster -n kube-system

root@VM-16-12-ubuntu:/home/ubuntu# kubectl get cluster -n kube-system
NAME           AGE
test-cluster   4m
```



### Config erebus RBAC

Since we want to use in-cluster access method, and the default service account does not have the permission to view Cluster resource, so we have to create our own.



First, create a ServiceAccount :

```bash
kubectl create serviceaccount cluster -n system
```

(The `aladua-system` namespace is hard-coded in the code now, in feature it can be customized by env)



Since we only need to `get` Cluster resource and Secret resource, so we can define a simple ClusterRole for us:

```bash
kubectl apply -f ${EREBUS_ROOT}/artifacts/deploy/in-cluster/clusterrole.yaml`
```


```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: cluster-resource-view
rules:
  - apiGroups:
      - "clusterregistry.k8s.io"
    resources:
      - clusters
    verbs:
      - get
  - apiGroups:
      - ""
    resources:
      - secrets
    verbs:
      - get

```



Then, bind the sa  to this ClusterRole

```bash
 kubectl create rolebinding my-sa-view --clusterrole=cluster-resource-view --serviceaccount=system:cluster --namespace=system
```



Finally ,when we deploy our erebus deploy, we need to specify our sa to it:

```
serviceAccountName: cluster
```

（of course if you want to pull image for erebus you need to configure docker secret for this serviceAccount)





# Links
1. [kubernetes/cluster-registry](https://github.com/kubernetes/cluster-registry)