# Cluster Resolvers

Erebus can be configured to use different cluster resolvers, users only need to provide the cluster name they want to use and configure a cluster resolver, Erebus will resolve the cluster info from the cluster name in the path and proxy the request to the actual kubernetes cluster.


## staticfile

This is the default resolver and it's easy to use and configure, just add the clusters info to a csv file in the following format:

```csv
<cluster-name-1>,<cluster-endpopint-1>
<cluster-name-2>,<cluster-endpoint-2>
```

comand args is `--staticfile=<csv-path>`



## furion

This resovler use Furion as clusters storage, just provide furion endpoint to the command lines args 

```bash
--resolver=furion --furion=<endpoint>
```

## clusterregistry
Please refer to [cluster_registry](./cluster_registry.md)
