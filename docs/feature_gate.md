# Feature Gates

Erebus use `apiserver`'s feature-gate, run `./erebus --help` to see 
the available features:

```text
     --feature-gates mapStringBool      A set of key=value pairs that describe feature gates for alpha/experimental features. Options are:
                                         AllAlpha=true|false (ALPHA - default=false)
                                         PrometheusMetrics=true|false (BETA - default=true)
```
                                   
`AllAlpha` is a built-in one and can be ignored, below is the detailed explanation about every feature

## PrometheusMetrics

This will record some metrics of all the request go through this proxy, and start a metrics api
at `:6060`。

The detailed metrics definition can be found at `k8s.io/apiserver/pkg/endpoints/metrics/metrics.go` 




