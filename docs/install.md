# How to run this proxy
[TOC]


## Binary
As simple as

`./erebus`

It use a few required args provided by default

* pem file path `--pem=hack/server.pem`
* key file path `--key=hack/server.key`
* cluster resolver `--resolver=staticfile`, means it use a static file(csv) as multi cluster data
* static file path: `--staticfile=artifacts/example/clusters.csv`, the path to the static file 

To see the full options, run `./erebus --help`

## Docker

The official docker image is located at : `index.alauda.cn/alaudaorg/erebus:<tag>`, and it's Dockerfile 
can be viewed at `<erebus_root>/artifacts/image/Dockerfile.gitlab`

The docker image can be seen as a thin wrapper around the binary, but it use `furion` as the default cluster 
resolver, and provide two env variable to customize this:

* `RESOLVER=furion`
* `FURION_ENDPOINT=http://furion:8080`

If you need to customize other options, just provide a different cmd for this docker image


## Kubernetes
The yaml files are located in `artifacts/deploy/*.yaml`。Feel free to change the envs about cluster resolver in the yaml files. 

If you want to use the `clusterregistry` resolver ,please check the [cluster_registry](./cluster_registry.md) doc.

Note:

1. The erebus deployment need to use a different serviceAccount than default in this scenario.
2. If use the `clusterregistry` resolver, erebus and cluster/rolebinding/secret/serviceaccount... resource must be in the same namespace. flag `--clusters-namespace` specific the namespace where erebus trying to read the Cluster resource. If you use another namespace than the default one(system), remember to change all the namespace field in the deploy YAMLs.



