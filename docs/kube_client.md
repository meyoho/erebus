# Client usage

[TOC]

## kubectl

This is an example kubectl config

```yaml
apiVersion: v1
clusters:
  - cluster:
      insecure-skip-tls-verify: true
      server: https://localhost:443/kubernetes/dev
    name: proxy
contexts:
  - context:
      cluster: proxy
      namespace: default
      user: admin
    name: proxy
current-context: proxy
kind: Config
preferences: {}
users:
  - name: admin
    user:
      token: <bearer-token>

```



Notes:

* `cluster.server`: append the `kubernetes/<cluster-name>` path to the server field
* Use client-certificate is not test yet



## Client Go
Users can use standard client-go package to access erebus to gain multi-cluster ability. 
It's similar to the kubectl config, just append the path suffix to the server(host) field, here is a sample code snippet:

```go
config := rest.Config{
	    // here cluster means cluster name
		Host: *endpoint + "/kubernetes/" + *cluster,
		BearerToken: *token,
		TLSClientConfig: rest.TLSClientConfig{
			Insecure: true,
		},
	}
	// create the clientset
	clientset, err := kubernetes.NewForConfig(&config)
	if err != nil {
		panic(err.Error())
	}

```

The full code can be found at [client-go-test](https://bitbucket.org/mathildetech/client-go-test)

