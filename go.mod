module alauda.io/erebus

go 1.12

require (
	github.com/Jeffail/gabs v1.3.1
	github.com/cssivision/reverseproxy v0.0.1
	github.com/emicklei/go-restful v2.9.5+incompatible // indirect
	github.com/evanphx/json-patch v4.2.0+incompatible // indirect
	github.com/gogo/protobuf v1.2.1 // indirect
	github.com/google/go-cmp v0.3.0
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/google/gofuzz v1.0.0 // indirect
	github.com/google/uuid v1.1.1 // indirect
	github.com/googleapis/gnostic v0.2.0 // indirect
	github.com/gsamokovarov/assert v0.0.0-20180414063448-8cd8ab63a335
	github.com/json-iterator/go v1.1.6 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/levigross/grequests v0.0.0-20190130132859-37c80f76a0da
	github.com/munnerz/goautoneg v0.0.0-20190414153302-2ae31c8b6b30 // indirect
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/prometheus/client_golang v0.9.3
	github.com/prometheus/procfs v0.0.0-20190519111021-9935e8e0588d // indirect
	github.com/spf13/cast v1.3.0
	github.com/spf13/pflag v1.0.3
	golang.org/x/crypto v0.0.0-20190513172903-22d7a77e9e5f // indirect
	golang.org/x/net v0.0.0-20190520210107-018c4d40a106 // indirect
	golang.org/x/oauth2 v0.0.0-20190517181255-950ef44c6e07 // indirect
	golang.org/x/sys v0.0.0-20190520201301-c432e742b0af // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/time v0.0.0-20190308202827-9d24e82272b4 // indirect
	google.golang.org/appengine v1.6.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/inf.v0 v0.9.1 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
	k8s.io/apiextensions-apiserver v0.0.0-20190515024537-2fd0e9006049 // indirect
	k8s.io/apimachinery v0.0.0-20190515023456-b74e4c97951f
	k8s.io/apiserver v0.0.0-20190515064100-fc28ef5782df
	k8s.io/client-go v0.0.0-20190515063710-7b18d6600f6b
	k8s.io/cluster-registry v0.0.7-0.20190313181743-eb099a004405
	k8s.io/component-base v0.0.0-20190515024022-2354f2393ad4
	k8s.io/klog v0.3.0
	k8s.io/utils v0.0.0-20190520173318-324c5df7d3f0 // indirect
)

replace golang.org/x/net => github.com/golang/net v0.0.0-20190514140710-3ec191127204

replace golang.org/x/crypto => github.com/golang/crypto v0.0.0-20190308221718-c2843e01d9a2

replace golang.org/x/text => github.com/golang/text v0.3.0

replace golang.org/x/sys => github.com/golang/sys v0.0.0-20181205085412-a5c9d58dba9a

replace golang.org/x/mobile => github.com/golang/mobile v0.0.0-20190509164839-32b2708ab171
