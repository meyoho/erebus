package main

import (
	"alauda.io/erebus/pkg/options"
	"alauda.io/erebus/pkg/proxy"
	"k8s.io/apiserver/pkg/server"
	"k8s.io/component-base/logs"
)

func main() {
	logs.InitLogs()
	defer logs.FlushLogs()

	stopCh := server.SetupSignalHandler()

	o := options.NewProxyOptions()
	p := proxy.NewProxy(o)
	p.RunMetricsServerIfEnabled()
	p.Run(stopCh)
}
