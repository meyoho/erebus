package bootstrap

import (
	"context"
	"errors"
	"regexp"
	"strings"

	"k8s.io/apiserver/pkg/authentication/authenticator"
	"k8s.io/apiserver/pkg/authentication/user"
)

type bootstrapTokenParser struct{}

//NewBootstrapTokenParser create a new bootstrapTokenParser
func NewBootstrapTokenParser() authenticator.Token {
	return &bootstrapTokenParser{}
}

//AuthenticateToken ...
// ref: https://kubernetes.io/docs/reference/access-authn-authz/bootstrap-tokens/
func (*bootstrapTokenParser) AuthenticateToken(ctx context.Context, token string) (*authenticator.Response, bool, error) {
	matched, _ := regexp.MatchString(`^[a-z0-9]{6}\.[a-z0-9]{16}$`, token)
	if !matched {
		return nil, false, errors.New("not a bootstrap token")
	}

	splits := strings.Split(token, ".")
	if len(splits) != 2 {
		return nil, false, errors.New("not a valid bootstrap token ")
	}

	userName := "system:bootstrap:" + splits[0]
	groups := []string{"system:bootstrappers"}

	resp := authenticator.Response{
		User: &user.DefaultInfo{
			Name:   userName,
			Groups: groups,
		},
	}
	return &resp, true, nil
}
