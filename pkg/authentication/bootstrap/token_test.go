package bootstrap

import (
	"context"
	"testing"

	"github.com/gsamokovarov/assert"
)

func TestBootstrapTokenParser(t *testing.T) {
	p := NewBootstrapTokenParser()

	data := []struct {
		token  string
		ok     bool
		name   string
		groups []string
		err    bool
	}{
		{
			"fuckfk.baidu12345679527",
			true,
			"system:bootstrap:fuckfk",
			[]string{"system:bootstrappers"},
			false,
		},
		{
			"a.b",
			false,
			"",
			nil,
			true,
		},
	}

	for _, item := range data {
		t.Run(item.token, func(t *testing.T) {
			resp, ok, err := p.AuthenticateToken(context.TODO(), item.token)
			assert.Equal(t, ok, item.ok)
			assert.Equal(t, err != nil, item.err)

			if resp != nil {
				assert.Equal(t, item.name, resp.User.GetName())
				assert.Equal(t, item.groups, resp.User.GetGroups())
			}

		})
	}

}
