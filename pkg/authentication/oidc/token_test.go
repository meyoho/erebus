package oidc

import (
	"encoding/json"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/gsamokovarov/assert"
)

var token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjBlOWVlOTAzYjNkNTFiMWY2ZDEzNjRlNjE5ZTY5N2UwZTQxNzEyZjMifQ.eyJpc3MiOiJodHRwczovLzExOC4yNC4yMjQuMTA1L2RleCIsInN1YiI6IkNpUXdPR0U0TmpnMFlpMWtZamc0TFRSaU56TXRPVEJoT1MwelkyUXhOall4WmpVME5qWVNCV3h2WTJGcyIsImF1ZCI6ImFsYXVkYS1hdXRoIiwiZXhwIjoxNTU5MzgxNjczLCJpYXQiOjE1NTkyOTUyNzMsIm5vbmNlIjoicm5nIiwiYXRfaGFzaCI6IjRzcExTZFVicWJWckpGZHVBMVltOUEiLCJlbWFpbCI6ImFkbWluQGFsYXVkYS5pbyIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJuYW1lIjoiYWRtaW4iLCJleHQiOnsiaXNfYWRtaW4iOnRydWUsImNvbm5faWQiOiJsb2NhbCJ9fQ.sDbgaZmh26t7Enxr3sSIlhS2l0UbFIpXyVzCnKmLxAupqyxteBOVSOPh_WRRtwg1_qDdvhEM4-eZhwPWY3NVBn6R2ewSlS0dVPTZJbTvaZf-YkqxqgEZRRfd26QoUf7N77QyWIXJhRAflKHtoJi2QZr9joIH14t-Wn6OFLa3KDyy0CxE1bru0Uf-5yn5tD3HAOtqpc6dJFgUJ4rBGGsjrrJFvPKtCaAeA--T97A0X0832a071TYA9wykKeZ1F8VetDO4t8CpyI3rNOlqsxMQ1-cLP9hz8u1Bko1IaR4xOV6odPS3WehS5ZuM9duXAgUJasY4_8Ug8BtNlJ4fQ2dh9g"
var result = `{
  "iss": "https://118.24.224.105/dex",
  "sub": "CiQwOGE4Njg0Yi1kYjg4LTRiNzMtOTBhOS0zY2QxNjYxZjU0NjYSBWxvY2Fs",
  "aud": "alauda-auth",
  "exp": 1559381673,
  "iat": 1559295273,
  "nonce": "rng",
  "at_hash": "4spLSdUbqbVrJFduA1Ym9A",
  "email": "admin@alauda.io",
  "email_verified": true,
  "name": "admin",
  "ext": {
    "is_admin": true,
    "conn_id": "local"
  }
}`

func loadJWT(data string) *jwtToken {
	var j jwtToken
	if err := json.Unmarshal([]byte(data), &j); err != nil {
		panic(err)
	}
	return &j
}

func TestParserToken(t *testing.T) {
	j, err := parseJWT(token)
	assert.Nil(t, err)

	// ....
	j.MetadataName = ""

	// t.Logf("fuck: %+v %+v", j, loadJWT(result))
	assert.True(t, cmp.Equal(*j, *loadJWT(result)))

	info := j.toUserInfo()
	assert.Equal(t, j.Email, info.GetName())
	assert.Equal(t, j.Groups, info.GetGroups())
}

func TestParserServiceAccountToken(t *testing.T) {
	saToken := "eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJjbHVzdGVycm9sZS1hZ2dyZWdhdGlvbi1jb250cm9sbGVyLXRva2VuLXA5ZDlmIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImNsdXN0ZXJyb2xlLWFnZ3JlZ2F0aW9uLWNvbnRyb2xsZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJlYTE5YmJhMS04MWYyLTExZTktOTQ3Mi01MjU0MDA3YjFiMjIiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06Y2x1c3RlcnJvbGUtYWdncmVnYXRpb24tY29udHJvbGxlciJ9.jI6M3ErK3mlAGk-cCtuhdS4H34-4Wl7yBGMWxOpqhaczHtlIi3H1dZHzDySNihTsy8kcpbJRhz-vkitYslKkqIKqbtkvUNTSAivXSxieSKbtb7jeCt1-6OUgoQTNRcYfpRK7Ur-2Y8XcRazcxr6i-tIODhljBmSd9mT32jLtyoDNsraE9o7-c4eEvA5DxkM6BlvpVHSh6cnBN4UhJ7qKo-M1g1ZSyAIGwN-Zd_0uQjcnAA5AqyzRUGQJ2AalXy5IGDescE2sL8mbOJqOHbB9PRNmI2vCcVzyhDtjmfR9o-EToPRvn8bUqqV3ulmznMGNM9SkLNUWUQCU1ZMkrP2ATg"
	j, err := parseJWT(saToken)
	assert.Nil(t, err)
	u := j.toUserInfo()

	expGroups := []string{
		"system:serviceaccounts",
		"system:serviceaccounts:kube-system",
	}

	assert.Equal(t, "system:serviceaccount:kube-system:clusterrole-aggregation-controller", u.GetName())
	assert.True(t, cmp.Equal(expGroups, u.GetGroups()))
	assert.Equal(t, "ea19bba1-81f2-11e9-9472-5254007b1b22", u.GetUID())

}
