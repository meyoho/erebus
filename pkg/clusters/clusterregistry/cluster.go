package clusterregistry

import (
	"errors"
	"time"

	"alauda.io/erebus/pkg/clusters"
	cache "github.com/patrickmn/go-cache"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/rest"
	"k8s.io/cluster-registry/pkg/apis/clusterregistry/v1alpha1"
	"k8s.io/cluster-registry/pkg/client/clientset/versioned"
)

// Resolver use ClusterRegistry to resolve cluster information.
// It's similar to furion, but we can use in-cluster client to retrieve the information from etcd
type Resolver struct {
	client versioned.Interface

	namespace string

	// Cache parsed it
	Cache *cache.Cache
}

// NewClusterRegistryResolver create a ClusterRegistry resolver ,panic if error happens
func NewClusterRegistryResolver(namespace string) *Resolver {
	// creates the in-cluster config
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
	}
	client := versioned.NewForConfigOrDie(config)
	return &Resolver{
		client:    client,
		namespace: namespace,
		Cache:     cache.New(3*time.Minute, 5*time.Minute),
	}
}

func (r *Resolver) transToClusterInfo(cr *v1alpha1.Cluster) (*clusters.Item, error) {
	var item clusters.Item

	eps := cr.Spec.KubernetesAPIEndpoints.ServerEndpoints
	if len(eps) > 0 {
		item.Endpoint = eps[0].ServerAddress
	} else {
		return nil, errors.New("no endpoint found for cluster")
	}
	// no use for now
	item.CABundle = cr.Spec.KubernetesAPIEndpoints.CABundle

	return &item, nil

}

// ResolveCluster get cluster info by name
// This should be easier compare to furion, because we can get individual cluster
func (r *Resolver) ResolveCluster(name string) (*clusters.Item, error) {
	data, ok := r.Cache.Get(name)
	if ok {
		return data.(*clusters.Item), nil
	}

	options := v1.GetOptions{}
	cr, err := r.client.ClusterregistryV1alpha1().Clusters(r.namespace).Get(name, options)
	if err != nil {
		return nil, err
	}

	item, err := r.transToClusterInfo(cr)
	if err != nil {
		return nil, err
	}
	r.Cache.SetDefault(name, item)
	return item, nil
}
