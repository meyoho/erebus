package clusterregistry

// Current we cannot use fake client provided by cluster-registry, so the test will only
// be some small ones
// issue: https://github.com/kubernetes/cluster-registry/issues/271

//
//import (
//	"bytes"
//	"github.com/gsamokovarov/assert"
//	"io/ioutil"
//	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
//	"k8s.io/apimachinery/pkg/runtime"
//	"k8s.io/apimachinery/pkg/util/yaml"
//	"k8s.io/cluster-registry/pkg/client/clientset/versioned/fake"
//	"testing"
//)
//
//func readYAMLData(path string) runtime.Object{
//	data ,_ :=ioutil.ReadFile(path)
//	decoder := yaml.NewYAMLOrJSONDecoder(bytes.NewReader(data), len(data))
//	var obj unstructured.Unstructured
//	if err := decoder.Decode(obj); err != nil {
//		panic(err)
//	}
//	return &obj
//}
//
//func TestClusterRegistryResolver(t *testing.T) {
//	objects := []runtime.Object{readYAMLData("../../../artifacts/deploy/clustergistry/example-cluster-registry")}
//	c := fake.NewSimpleClientset(objects...)
//
//
//	r := Resolver{
//		client: c,
//	}
//	result ,err := r.ResolveCluster("test-cluster")
//	assert.Nil(err)
//	assert.NotNil(result)
//
//}
