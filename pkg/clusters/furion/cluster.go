package furion

import (
	"fmt"
	"log"
	"time"

	cache "github.com/patrickmn/go-cache"

	"alauda.io/erebus/pkg/clusters"
	"github.com/Jeffail/gabs"
	"github.com/levigross/grequests"
	"github.com/spf13/cast"
)

const apiVersion = "v2"
const defaultTimeout = 3

// Resolver is a furion config for multi cluster management
type Resolver struct {
	Endpoint string

	Cache *cache.Cache
}

// NewFurionResolver generate a new cluster resolver
func NewFurionResolver(endpoint string) clusters.Resolver {
	return &Resolver{Endpoint: endpoint, Cache: cache.New(3*time.Minute, 5*time.Minute)}
}

//ResolveCluster ...
func (c *Resolver) ResolveCluster(name string) (*clusters.Item, error) {

	data, ok := c.Cache.Get(name)
	if ok {
		return data.(*clusters.Item), nil
	}

	if err := c.SyncRegions(); err != nil {
		return nil, err
	}

	data, ok = c.Cache.Get(name)
	if ok {
		return data.(*clusters.Item), nil
	}

	return nil, fmt.Errorf("get cluster %s error", name)
}

// SyncRegions list regions from furion and set to cache
func (c *Resolver) SyncRegions() error {
	url := fmt.Sprintf("%s/%s/regions", c.Endpoint, apiVersion)
	resp, err := grequests.Get(url, &grequests.RequestOptions{
		RequestTimeout: time.Duration(defaultTimeout) * time.Second})
	if err != nil {
		return err
	}

	list, err := gabs.ParseJSON([]byte(resp.String()))
	if err != nil {
		return err
	}

	children, err := list.Children()
	if err != nil {
		return err
	}

	for _, child := range children {
		item, err := getKubernetesConfig(child)
		if err != nil {
			log.Printf("ERROR get region: %+v", err)
			continue
		}

		name, err := getPathFromJSON(child, "name")
		if err != nil {
			log.Printf("ERROR get cluster name")
			continue
		}
		c.Cache.Set(name, item, cache.DefaultExpiration)
	}
	return nil

}

func getPathFromJSON(jsonData *gabs.Container, path string) (string, error) {
	result := jsonData.Path(path).Data()
	value, ok := result.(string)
	if ok {
		return value, nil
	}
	number, ok := result.(float64)
	if ok {
		return cast.ToString(number), nil
	}

	return "", fmt.Errorf("get path %s data for region error", path)

}

func getKubernetesConfig(data *gabs.Container) (*clusters.Item, error) {
	ep, err := getPathFromJSON(data, "attr.kubernetes.endpoint")
	if err != nil {
		return nil, err
	}

	token, err := getPathFromJSON(data, "attr.kubernetes.token")
	if err != nil {
		return nil, err
	}

	return &clusters.Item{
		Endpoint: ep,
		Token:    token,
	}, nil
}
