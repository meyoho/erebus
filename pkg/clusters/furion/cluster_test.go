package furion

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gsamokovarov/assert"
)

func TestFurionResolver(t *testing.T) {
	data := `[{
      "id": "9636741d-63bb-465e-8483-af69d0d41228",
      "platform_version": "v4",
      "name": "dev",
      "attr": {
        "kubernetes": {
          "endpoint": "https://139.219.56.257:6443",
          "token": "7dd33a.84c08dc99025557f",
          "version": "1.7.3"
        }
      }
    }]`

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprintln(w, data)
	}))
	defer ts.Close()

	f := NewFurionResolver(ts.URL)
	result, err := f.ResolveCluster("dev")
	assert.Nil(t, err)
	assert.Equal(t, result.Endpoint, "https://139.219.56.257:6443")
	assert.Equal(t, result.Token, "7dd33a.84c08dc99025557f")

}
