package clusters

// Item contains information about a single cluster
type Item struct {
	// Endpoint is the kubernetes api server address
	Endpoint string

	// Token is the admin for kubernetes, currently we don't use it in the proxy, it may be deleted
	// in the feature
	Token string

	// CABundle ...
	CABundle []byte
}

// ResolverType ...
type ResolverType string

const (
	// ResolverFurion use furion
	ResolverFurion ResolverType = "furion"
	// ResolverClusterRegistry use clusterregistry
	ResolverClusterRegistry ResolverType = "clusterregistry"
	// ResolverStaticFile use staticfile
	ResolverStaticFile ResolverType = "staticfile"
)

// Resolver resolve cluster info from it's name
type Resolver interface {
	ResolveCluster(name string) (*Item, error)
}
