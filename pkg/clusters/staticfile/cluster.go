package staticfile

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"sync"

	"alauda.io/erebus/pkg/clusters"
)

//Resolver resolve clusters information from a csv static_file, mainly for debug
type Resolver struct {
	Path string

	Clusters *sync.Map
}

//Get ...
func (r *Resolver) Get(name string) (*clusters.Item, bool) {
	v, ok := r.Clusters.Load(name)
	if !ok {
		return nil, false
	}
	item, ok := v.(*clusters.Item)
	return item, ok
}

//ResolveCluster ...
func (r *Resolver) ResolveCluster(name string) (*clusters.Item, error) {
	data, ok := r.Get(name)
	if ok {
		return data, nil
	}

	return nil, fmt.Errorf("cluster not found")
}

//NewStaticFileResolver create a new cluster resolver
func NewStaticFileResolver(path string) clusters.Resolver {
	c, err := readCSVToClusters(path)
	if err != nil {
		panic(err)
	}

	return &Resolver{
		Path:     path,
		Clusters: c,
	}
}

func readCSVToClusters(filePath string) (*sync.Map, error) {
	// Load a csv static_file.
	f, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}

	c := sync.Map{}

	// Create a new reader.
	r := csv.NewReader(bufio.NewReader(f))
	for {
		record, err := r.Read()
		// Stop at EOF.
		if err == io.EOF {
			break
		}

		item := clusters.Item{}
		if len(record) > 1 {
			item.Endpoint = record[1]
		}
		if len(record) > 2 {
			item.Token = record[2]
		}

		c.Store(record[0], &item)

	}
	return &c, nil
}
