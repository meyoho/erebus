package staticfile

import (
	"testing"

	"github.com/gsamokovarov/assert"
)

func TestStaticFileResolver(t *testing.T) {
	path := "../../../test/fixtures/clusters.csv"
	r := NewStaticFileResolver(path)
	item, err := r.ResolveCluster("dev")
	if err != nil {
		t.Error("resolver error")
	}
	assert.Equal(t, item.Endpoint, "https://127.0.0.1:6443")
}
