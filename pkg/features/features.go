package features

import (
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/component-base/featuregate"
)

var (
	// Note: This block copied from: k8s.io/apierver/pkg/util/feature/feature_gate.go, it defined the
	// same vars but init it with a bunch of built-in feature flags, to avoid this, we have to define
	// our own.

	// DefaultMutableFeatureGate is a mutable version of DefaultFeatureGate.
	// Only top-level commands/options setup and the k8s.io/component-base/featuregate/testing package should make use of this.
	// Tests that need to modify feature gates for the duration of their test should use:
	//   defer featuregatetesting.SetFeatureGateDuringTest(t, utilfeature.DefaultFeatureGate, features.<FeatureName>, <value>)()
	DefaultMutableFeatureGate featuregate.MutableFeatureGate = featuregate.NewFeatureGate()

	// DefaultFeatureGate is a shared global FeatureGate.
	// Top-level commands/options setup that needs to modify this feature gate should use DefaultMutableFeatureGate.
	DefaultFeatureGate featuregate.FeatureGate = DefaultMutableFeatureGate
)

const (
	//PrometheusMetrics enable support for prometheus metrics
	PrometheusMetrics featuregate.Feature = "PrometheusMetrics"
)

func init() {
	runtime.Must(DefaultMutableFeatureGate.Add(defaultProxyFeatureGates))
}

// defaultProxyFeatureGates defines all the features we will use in this proxy
var defaultProxyFeatureGates = map[featuregate.Feature]featuregate.FeatureSpec{
	PrometheusMetrics: {Default: true, PreRelease: featuregate.Beta},
}
