package filters

import (
	"net/http"

	"alauda.io/erebus/pkg/util/api"
	"k8s.io/klog"
)

//withAccessToken copy access token from query args to auth headers if not exist.
// In some situations, user(browser?) can not set header value, this is a simple workaround
func withAccessToken(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		accessToken := req.URL.Query().Get("access-token")
		authHeader := req.Header.Get(api.AuthorizationHeaderKey)

		if accessToken != "" && authHeader == "" {
			klog.V(7).Infof("found access token in query, insert to header")
			req.Header.Set(api.AuthorizationHeaderKey, "Bearer "+accessToken)
		}
		handler.ServeHTTP(w, req)
	})
}
