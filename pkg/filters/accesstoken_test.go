package filters

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"alauda.io/erebus/pkg/util/api"
	"github.com/gsamokovarov/assert"
)

func TestAccessTokenFilter(t *testing.T) {
	token := "nothing-is-true"

	req, err := http.NewRequest("GET", "fk?access-token="+token, nil)
	if err != nil {
		t.Fatal(err)
	}
	// req.URL.Query().Set("access-token", token)

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()

	t.Run("working-one", func(t *testing.T) {
		testHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			auth := r.Header.Get(api.AuthorizationHeaderKey)
			assert.Equal(t, "Bearer "+token, auth)
		})
		h := withAccessToken(testHandler)
		h.ServeHTTP(rr, req)
	})

	t.Run("failed-one", func(t *testing.T) {
		req.Header.Set(api.AuthorizationHeaderKey, "fk")
		testHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			auth := r.Header.Get(api.AuthorizationHeaderKey)
			assert.NotEqual(t, "Bearer "+token, auth)
		})
		h := withAccessToken(testHandler)
		h.ServeHTTP(rr, req)
	})

}
