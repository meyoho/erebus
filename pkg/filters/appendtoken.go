package filters

import (
	"net/http"

	"alauda.io/erebus/pkg/util/api"
)

//withRewriteOriginPath restore the correct path for the backend kubernetes api server.
// This will be called after the audit recording
func withAppendToken(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		token, ok := req.Context().Value(api.ProxyOriginToken).(string)
		if !ok {
			handler.ServeHTTP(w, req)
			return
		}

		req.Header.Set(api.AuthorizationHeaderKey, token)
		handler.ServeHTTP(w, req)
	})
}
