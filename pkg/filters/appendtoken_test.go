package filters

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"alauda.io/erebus/pkg/util/api"
	"github.com/gsamokovarov/assert"
)

func TestAppendToken(t *testing.T) {
	token := "nothing-is-true"

	req, err := http.NewRequest("GET", "fk", nil)
	if err != nil {
		t.Fatal(err)
	}

	// req.URL.Query().Set("access-token", token)

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()

	t.Run("failed-one", func(t *testing.T) {
		testHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			assert.Equal(t, req.Header.Get(api.AuthorizationHeaderKey), "")
		})
		h := withAppendToken(testHandler)
		h.ServeHTTP(rr, req)
	})

	t.Run("working-one", func(t *testing.T) {
		req = req.WithContext(context.WithValue(req.Context(), api.ProxyOriginToken, token))
		testHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			assert.Equal(t, req.Header.Get(api.AuthorizationHeaderKey), token)
		})
		h := withAppendToken(testHandler)
		h.ServeHTTP(rr, req)
	})

}
