package filters

import (
	"net/http"

	"k8s.io/apiserver/pkg/endpoints/filters"
	"k8s.io/apiserver/pkg/server"
	genericfilters "k8s.io/apiserver/pkg/server/filters"
)

//DefaultBuildHandlerChain add all the handler plugins
// mainly copied from: vendor/k8s.io/apiserver/pkg/server/config.go
func DefaultBuildHandlerChain(apiHandler http.Handler, c *server.Config) http.Handler {
	// record audit, temp change path and write back
	handler := withRewriteOriginPath(apiHandler)
	handler = withAccessToken(handler)
	handler = withAppendToken(handler)
	handler = filters.WithAudit(handler, c.AuditBackend, c.AuditPolicyChecker, c.LongRunningFunc)
	handler = withOriginPath(handler)

	// record metrics
	handler = withMetrics(handler, c.RequestInfoResolver)

	// auth. The failedHandler is used for auth failed, this should be the backend kubernetes apiserver's job
	// but the signature need this ,so we create an nop-handler to do this and proxy pass it. The default one
	// enforce a bearer token( from the configuration).
	// failedHandler := filters.Unauthorized(c.Serializer, c.Authentication.SupportsBasicAuth)
	failedHandler := Nothing(handler)
	failedHandler = filters.WithFailedAuthenticationAudit(failedHandler, c.AuditBackend, c.AuditPolicyChecker)
	handler = filters.WithAuthentication(handler, c.Authentication.Authenticator, failedHandler, c.Authentication.APIAudiences)
	// system
	handler = genericfilters.WithTimeoutForNonLongRunningRequests(handler, c.LongRunningFunc, c.RequestTimeout)
	handler = genericfilters.WithWaitGroup(handler, c.LongRunningFunc, c.HandlerChainWaitGroup)
	handler = filters.WithRequestInfo(handler, c.RequestInfoResolver)
	handler = genericfilters.WithPanicRecovery(handler)
	return handler
}
