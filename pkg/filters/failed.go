package filters

import (
	"net/http"
)

//Nothing is an empty handler, may be this is unneeded?
func Nothing(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		handler.ServeHTTP(w, req)
	})
}
