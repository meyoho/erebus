package filters

import (
	"net/http"
	"strings"

	"alauda.io/erebus/pkg/features"
	"alauda.io/erebus/pkg/util/api"
	"k8s.io/apiserver/pkg/endpoints/metrics"
	"k8s.io/apiserver/pkg/endpoints/request"
	"k8s.io/klog"
)

func withMetrics(handler http.Handler, r request.RequestInfoResolver) http.Handler {
	if !features.DefaultFeatureGate.Enabled(features.PrometheusMetrics) {
		return handler
	}

	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		requestInfo, err := r.NewRequestInfo(req)
		if err != nil || requestInfo == nil {
			klog.Warningf("record request error: %+v %+v", requestInfo, err)

			handler.ServeHTTP(w, req)

		} else {
			f := func(w http.ResponseWriter, req *http.Request) {
				handler.ServeHTTP(w, req)
			}

			h := metrics.InstrumentHandlerFunc(strings.ToUpper(requestInfo.Verb),
				requestInfo.APIGroup,
				requestInfo.APIVersion,
				requestInfo.Resource,
				requestInfo.Subresource,
				metrics.CleanScope(requestInfo),
				api.ServerComponent,
				f,
			)
			h(w, req)
		}
	})
}
