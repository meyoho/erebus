package filters

import (
	"context"
	"net/http"

	"alauda.io/erebus/pkg/util/api"
)

//withOriginPath temporal rewrite request url path to the on with cluster name prefix
// mainly used for audit, after audit recording, this process will be reversed
func withOriginPath(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		path, ok := req.Context().Value(api.ProxyPath).(string)
		if !ok {
			handler.ServeHTTP(w, req)
			return
		}
		target := req.URL.Path
		req.URL.Path = path
		req = req.WithContext(context.WithValue(req.Context(), api.ProxyPath, target))
		handler.ServeHTTP(w, req)
	})
}

//withRewriteOriginPath restore the correct path for the backend kubernetes api server.
// This will be called after the audit recording
func withRewriteOriginPath(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		path, ok := req.Context().Value(api.ProxyPath).(string)
		if !ok {
			handler.ServeHTTP(w, req)
			return
		}

		req.URL.Path = path
		handler.ServeHTTP(w, req)
	})
}
