package filters

import (
	"context"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"alauda.io/erebus/pkg/util/api"
	"github.com/gsamokovarov/assert"
)

// e.g. http.HandleFunc("/health-check", HealthCheckHandler)
func HealthCheckHandler(w http.ResponseWriter, r *http.Request) {
	// A very simple health check.
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")

	// In the future we could report back on the status of our DB, or our cache
	// (e.g. Redis) by performing a simple PING, and include them in the response.
	io.WriteString(w, `{"alive": true}`)
}

func TestProxyPathFilter(t *testing.T) {
	// Create a request to pass to our handler. We don't have any query parameters for now, so we'll
	// pass 'nil' as the third parameter.

	targetPath := "/api"
	originPath := "/kubernetes/local/api"

	req, err := http.NewRequest("GET", targetPath, nil)
	if err != nil {
		t.Fatal(err)
	}
	req = req.WithContext(context.WithValue(req.Context(), api.ProxyPath, originPath))

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()

	t.Run("origin-path-handler", func(t *testing.T) {
		testHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if val, ok := r.Context().Value(api.ProxyPath).(string); !ok {
				t.Errorf("%s not in context", api.ProxyPath)
			} else {
				assert.Equal(t, targetPath, val)
			}
		})
		h := withOriginPath(testHandler)
		h.ServeHTTP(rr, req)
		assert.Equal(t, originPath, req.URL.Path)
	})

	t.Run("rewrite-path-handler", func(t *testing.T) {
		req = req.WithContext(context.WithValue(req.Context(), api.ProxyPath, targetPath))
		h := withRewriteOriginPath(http.HandlerFunc(HealthCheckHandler))
		h.ServeHTTP(rr, req)
		assert.Equal(t, targetPath, req.URL.Path)
	})

}
