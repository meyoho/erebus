package options

import (
	"fmt"

	"alauda.io/erebus/pkg/clusters"
	"alauda.io/erebus/pkg/clusters/clusterregistry"
	"alauda.io/erebus/pkg/clusters/furion"
	"alauda.io/erebus/pkg/clusters/staticfile"
	"alauda.io/erebus/pkg/server"
	"github.com/spf13/pflag"
	"k8s.io/klog"
)

// Version will be replaced by Makefile when build
var Version = ""

// GenericOptions contains options from flags
type GenericOptions struct {
	TLSPemPath          string
	TLSKeyPath          string
	Proto               string
	ClusterResolverType string

	StaticResolverFilePath string
	FurionEndpoint         string

	// ClustersNamespace specify which namespace the Cluster resource lives in
	ClustersNamespace string

	// Port is the port the proxy will listen on
	Port string

	PrintVersion bool
}

//NewGenericOptions create options from flags
func NewGenericOptions() *GenericOptions {
	genericOptions := new(GenericOptions)
	return genericOptions
}

// Validate validate this config
func (o *GenericOptions) Validate() []error {
	var errors []error
	if o.Proto != "http" && o.Proto != "https" {
		errors = append(errors, fmt.Errorf("protocol must be either http or https"))
	}
	return errors
}

//AddFlags ...
func (o *GenericOptions) AddFlags(fs *pflag.FlagSet) {
	fs.StringVar(&o.TLSPemPath, "pem", "hack/server.pem", "Path to pem file")
	fs.StringVar(&o.TLSKeyPath, "key", "hack/server.key", "Path to key file")
	fs.StringVar(&o.Proto, "proto", "https", "Proxy protocol (http or https)")
	fs.StringVar(&o.Port, "port", "443", "Listen on port")

	fs.StringVar(&o.ClusterResolverType, "resolver", "staticfile",
		"Cluster resolver type, support furion/clusterregistry/staticfile")
	fs.StringVar(&o.StaticResolverFilePath, "staticfile", "artifacts/example/clusters.csv",
		"Default clusters file, working when resolver=staticfile")
	fs.StringVar(&o.FurionEndpoint, "furion", "http://furion:8080",
		"Furion endpoint, working when resolver=furion")
	fs.StringVar(&o.ClustersNamespace, "clusters-namespace", "system",
		"Specific which namespace the Cluster resource lives in if use clusterregistry resolver")

	fs.BoolVar(&o.PrintVersion, "version", false, "print version")

}

//ApplyTo ...
func (o *GenericOptions) ApplyTo(config *server.Config) error {
	config.GenericConfig.ClusterResolverType = clusters.ResolverType(o.ClusterResolverType)

	r := GetResolver(o)
	if r == nil {
		return fmt.Errorf("unsupported cluster resolver: %s", o.ClusterResolverType)
	}
	klog.Infof("use cluster resolver: %s", o.ClusterResolverType)
	config.GenericConfig.ClusterResolver = r

	config.HTTPServerConfig.Port = o.Port
	config.HTTPServerConfig.Proto = o.Proto
	config.HTTPServerConfig.TLSKeyPath = o.TLSKeyPath
	config.HTTPServerConfig.TLSPemPath = o.TLSPemPath

	return nil
}

// GetResolver get a resolver based on type
func GetResolver(o *GenericOptions) clusters.Resolver {
	t := clusters.ResolverType(o.ClusterResolverType)
	switch t {
	case clusters.ResolverStaticFile:
		return staticfile.NewStaticFileResolver(o.StaticResolverFilePath)
	case clusters.ResolverFurion:
		return furion.NewFurionResolver(o.FurionEndpoint)
	case clusters.ResolverClusterRegistry:
		return clusterregistry.NewClusterRegistryResolver(o.ClustersNamespace)
	default:
		return nil
	}
}
