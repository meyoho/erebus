package options

import (
	"flag"

	"alauda.io/erebus/pkg/features"
	"alauda.io/erebus/pkg/server"
	"github.com/spf13/pflag"
)

//ProxyOptions ...
type ProxyOptions struct {
	GenericOptions *GenericOptions
	ServerOptions  *ServerOptions
}

//NewProxyOptions ...
func NewProxyOptions() *ProxyOptions {
	o := &ProxyOptions{
		GenericOptions: NewGenericOptions(),
		ServerOptions:  NewServerOptions(),
	}

	return o
}

//Validate ...
func (o *ProxyOptions) Validate() []error {
	var errors []error
	errors = append(errors, o.GenericOptions.Validate()...)
	errors = append(errors, o.ServerOptions.Validate()...)
	return errors
}

//ApplyTo ...
func (o *ProxyOptions) ApplyTo(config *server.Config) error {
	if err := o.ServerOptions.ApplyTo(config); err != nil {
		return err
	}

	if err := o.GenericOptions.ApplyTo(config); err != nil {
		return err
	}
	return nil
}

//AddFlags ...
func (o *ProxyOptions) AddFlags(fs *pflag.FlagSet) {
	o.GenericOptions.AddFlags(fs)
	o.ServerOptions.AddFlags(fs)

	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	features.DefaultMutableFeatureGate.AddFlag(pflag.CommandLine)
}
