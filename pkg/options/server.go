package options

import (
	"alauda.io/erebus/pkg/filters"
	"alauda.io/erebus/pkg/server"
	"github.com/spf13/pflag"
	serveroptions "k8s.io/apiserver/pkg/server/options"
)

//ServerOptions ...
type ServerOptions struct {
	Audit *serveroptions.AuditOptions
}

//NewServerOptions ...
func NewServerOptions() *ServerOptions {
	return &ServerOptions{
		Audit: serveroptions.NewAuditOptions(),
	}
}

//AddFlags ...
func (o *ServerOptions) AddFlags(fs *pflag.FlagSet) {
	o.Audit.AddFlags(fs)
}

//ApplyTo ...
func (o *ServerOptions) ApplyTo(config *server.Config) error {
	if err := o.Audit.ApplyTo(&config.Config, nil, nil, nil, nil); err != nil {
		return err
	}

	config.BuildHandlerChainFunc = filters.DefaultBuildHandlerChain

	return nil
}

//Validate ...
func (o *ServerOptions) Validate() []error {
	var errors []error
	errors = append(errors, o.Audit.Validate()...)
	return errors
}
