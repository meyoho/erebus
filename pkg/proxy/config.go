package proxy

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	cache "github.com/patrickmn/go-cache"

	"alauda.io/erebus/pkg/options"
	"alauda.io/erebus/pkg/server"
	"github.com/spf13/pflag"
	"k8s.io/klog"
)

// Proxy hold all the info abouts this proxy
// Besides the Config, others are the information can not be configured outside
type Proxy struct {
	Config server.Config

	// HostProxy is a cache for ReverseProxy, since we use host as map key, it can be cached forever,
	// sync.Map is also suitable for this situation
	HostProxy *cache.Cache
}

// NewProxy create a default proxy from generic options
// panic if error occurs
func NewProxy(o *options.ProxyOptions) *Proxy {
	c := server.NewConfig()

	o.AddFlags(pflag.CommandLine)
	pflag.Parse()
	if o.GenericOptions.PrintVersion {
		fmt.Println("Version: ", options.Version)
		os.Exit(0)
	}

	if errs := o.Validate(); errs != nil {
		panic(errs)
	}

	if err := o.ApplyTo(c); err != nil {
		panic(err)
	}

	p := &Proxy{
		Config:    *c,
		HostProxy: cache.New(1*time.Minute, 3*time.Minute),
	}

	return p
}

// Run run the proxy server
func (p *Proxy) Run(stopCh <-chan struct{}) {
	g := p.Config.HTTPServerConfig
	s := &http.Server{
		Addr:    ":" + g.Port,
		Handler: p,
		// Disable HTTP/2.
		TLSNextProto: make(map[string]func(*http.Server, *tls.Conn, http.Handler)),
	}
	if p.Config.AuditBackend != nil {
		if err := p.Config.AuditBackend.Run(stopCh); err != nil {
			log.Fatal("init audit backend error: ", err)
		}
	}

	klog.Infof("Starting %s proxy server on: %s ", g.Proto, s.Addr)
	if g.Proto == "http" {
		log.Fatal(s.ListenAndServe())
	} else {
		log.Fatal(s.ListenAndServeTLS(g.TLSPemPath, g.TLSKeyPath))
	}
}
