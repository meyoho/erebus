package proxy

import (
	"net/http"

	"alauda.io/erebus/pkg/features"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"k8s.io/apiserver/pkg/endpoints/metrics"
	"k8s.io/klog"
)

// RunMetricsServerIfEnabled will run a metrics server if enabled
func (p *Proxy) RunMetricsServerIfEnabled() {
	if !features.DefaultFeatureGate.Enabled(features.PrometheusMetrics) {
		return
	}
	klog.Info("Run metrics server on 6060")

	metrics.Register()

	server := http.Server{
		Addr:    ":6060",
		Handler: promhttp.Handler(),
	}

	go func() {
		klog.Fatal(server.ListenAndServe())
	}()
}
