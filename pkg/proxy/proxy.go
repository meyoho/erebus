package proxy

import (
	"context"
	"crypto/tls"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/spf13/cast"

	"alauda.io/erebus/pkg/request"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/klog"

	"alauda.io/erebus/pkg/util/api"
	"github.com/cssivision/reverseproxy"
)

func (p *Proxy) setProxy(host string, proxy *reverseproxy.ReverseProxy) {
	p.HostProxy.SetDefault(host, proxy)
}

func (p *Proxy) getProxy(host string) (*reverseproxy.ReverseProxy, bool) {
	v, ok := p.HostProxy.Get(host)
	if !ok {
		return nil, false
	}

	proxy, ok := v.(*reverseproxy.ReverseProxy)
	return proxy, ok

}

func (p *Proxy) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	klog.V(9).Infof("receive header: %+v", r.Header)

	// a health check path
	if r.URL.Path == "/_health" {
		w.Write([]byte("OK"))
		return
	}

	pathSplits := strings.Split(r.URL.Path, "/")
	if len(pathSplits) < 3 || (len(pathSplits) > 1 && pathSplits[1] != api.ProxyPathPrefix) {
		klog.Errorf("bad path: %s", r.URL.Path)
		err := errors.NewBadRequest("bad path")
		request.WriteError(p.Config.RequestScope, err, w, r)
		return
	}

	clusterName := strings.Split(r.URL.Path, "/")[2]

	cluster, err := p.Config.GenericConfig.ClusterResolver.ResolveCluster(clusterName)
	if err != nil {
		request.WriteError(p.Config.RequestScope, errors.NewBadRequest(err.Error()), w, r)
		return
	}

	target, _ := url.Parse(cluster.Endpoint)
	r = rewritePath(r, clusterName)

	klog.Infof("proxy to cluster %s with endpoint: %s%s", clusterName, cluster.Endpoint, r.URL.String())

	timeout := 300
	rawTimeout := r.URL.Query().Get("timeoutSeconds")
	if rawTimeout != "" {
		t, err := cast.ToIntE(rawTimeout)
		if err != nil {
			klog.Error("parse timeoutSeconds query error:", err)
		} else {
			timeout = t
		}
	}

	if isUpgradeRequest(r) {
		klog.Info("receive upgrade request")
		if strings.HasPrefix(cluster.Endpoint, "https") {
			r.Host = cluster.Endpoint[8:]
			r.URL.Host = r.Host
			r.URL.Scheme = "https"
		} else {
			r.Host = cluster.Endpoint[7:]
			r.URL.Host = r.Host
			r.URL.Scheme = "http"
		}

		upgradeProxy := upgradeProxy{
			insecure:        true,
			backendAddr:     r.URL,
			tlsClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		// upgradeProxy.ServeHTTP(w, r)
		p.Handle(&upgradeProxy, w, r)
		return
	}

	klog.V(9).Infof("target host: %s", target.Host)

	if fn, ok := p.getProxy(target.Host); ok {
		fn.Timeout = time.Duration(timeout)
		p.Handle(fn, w, r)
		return
	}

	klog.Info("generate new reverse proxy for: ", target.Host)

	proxy := reverseproxy.NewReverseProxy(target)
	// just add 10s for erebus
	proxy.Timeout = time.Duration(timeout) + 10
	// avoid to cause watch api stuck
	proxy.FlushInterval = 100 * time.Millisecond

	proxy.Transport = &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true}, // client uses self-signed cert
	}

	p.setProxy(target.Host, proxy)
	p.Handle(proxy, w, r)
}

// Handle do the actually handler some extra filters
func (p *Proxy) Handle(handler http.Handler, w http.ResponseWriter, req *http.Request) {
	handler = p.Config.BuildHandlerChainFunc(handler, &p.Config.Config)
	handler.ServeHTTP(w, req)
}

func rewritePath(req *http.Request, cluster string) *http.Request {
	// preserve origin path to be used in audit
	ctx := req.Context()
	ctx = context.WithValue(ctx, api.ProxyPath, req.URL.Path)
	ctx = context.WithValue(ctx, api.ProxyOriginToken, req.Header.Get(api.AuthorizationHeaderKey))
	req = req.WithContext(ctx)
	req.URL.Path = req.URL.Path[len("//"+api.ProxyPathPrefix+cluster):]
	klog.V(9).Infof("after rewrite path: %s", req.URL.Path)
	return req
}
