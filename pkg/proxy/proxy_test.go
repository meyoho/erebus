package proxy

import (
	"io/ioutil"
	"net"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"alauda.io/erebus/pkg/clusters"
	"alauda.io/erebus/pkg/options"
	"alauda.io/erebus/pkg/server"
	"alauda.io/erebus/pkg/util/api"
	"github.com/gsamokovarov/assert"
	cache "github.com/patrickmn/go-cache"
)

var authToken = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjBlOWVlOTAzYjNkNTFiMWY2ZDEzNjRlNjE5ZTY5N2UwZTQxNzEyZjMifQ.eyJpc3MiOiJodHRwczovLzExOC4yNC4yMjQuMTA1L2RleCIsInN1YiI6IkNpUXdPR0U0TmpnMFlpMWtZamc0TFRSaU56TXRPVEJoT1MwelkyUXhOall4WmpVME5qWVNCV3h2WTJGcyIsImF1ZCI6ImFsYXVkYS1hdXRoIiwiZXhwIjoxNTU5MzgxNjczLCJpYXQiOjE1NTkyOTUyNzMsIm5vbmNlIjoicm5nIiwiYXRfaGFzaCI6IjRzcExTZFVicWJWckpGZHVBMVltOUEiLCJlbWFpbCI6ImFkbWluQGFsYXVkYS5pbyIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJuYW1lIjoiYWRtaW4iLCJleHQiOnsiaXNfYWRtaW4iOnRydWUsImNvbm5faWQiOiJsb2NhbCJ9fQ.sDbgaZmh26t7Enxr3sSIlhS2l0UbFIpXyVzCnKmLxAupqyxteBOVSOPh_WRRtwg1_qDdvhEM4-eZhwPWY3NVBn6R2ewSlS0dVPTZJbTvaZf-YkqxqgEZRRfd26QoUf7N77QyWIXJhRAflKHtoJi2QZr9joIH14t-Wn6OFLa3KDyy0CxE1bru0Uf-5yn5tD3HAOtqpc6dJFgUJ4rBGGsjrrJFvPKtCaAeA--T97A0X0832a071TYA9wykKeZ1F8VetDO4t8CpyI3rNOlqsxMQ1-cLP9hz8u1Bko1IaR4xOV6odPS3WehS5ZuM9duXAgUJasY4_8Ug8BtNlJ4fQ2dh9g"

func getBody(resp *http.Response, t *testing.T) string {
	t.Helper()
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Errorf("read resp error: %s", err.Error())
	}
	return string(bodyBytes)
}

//TODO: check on the tls problem
func TestProxyHandler(t *testing.T) {
	// set backend server
	backendResponse := `{"kind":"APIVersions","versions":["v1"],"serverAddressByClientCIDRs":[{"clientCIDR":"0.0.0.0/0","serverAddress":"192.168.65.3:6443"}]}`
	const backendStatus = 200

	backend := httptest.NewUnstartedServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(backendStatus)
		w.Write([]byte(backendResponse))

	}))
	// create a listener with the desired port.
	l, err := net.Listen("tcp", "127.0.0.1:9527")
	if err != nil {
		panic(err)
	}
	backend.Listener.Close()
	backend.Listener = l
	backend.StartTLS()
	// set url after start
	backend.URL = "https://127.0.0.1:9527/api"
	defer backend.Close()

	// set frontend server
	o := options.ProxyOptions{
		GenericOptions: &options.GenericOptions{
			ClusterResolverType:    string(clusters.ResolverStaticFile),
			StaticResolverFilePath: "../../artifacts/example/clusters.csv",
		},
		ServerOptions: options.NewServerOptions(),
	}
	c := server.NewConfig()
	if err := o.ApplyTo(c); err != nil {
		t.Error("init proxy error: ", err)
	}

	handler := &Proxy{
		Config:    *c,
		HostProxy: cache.New(1*time.Minute, 3*time.Minute),
	}

	frontend := httptest.NewServer(handler)
	baseFrontURL := frontend.URL

	t.Run("200-ok", func(t *testing.T) {
		frontend.URL = baseFrontURL + "/kubernetes/test/api"
		// disable logs in unit test
		// log.SetOutput(ioutil.Discard)
		//klog.SetOutput(ioutil.Discard)
		//resp, err := http.Get(frontend.URL + "/kubernetes/test/api")

		// a simple http.Get can also work... the token is not working for now
		req, err := http.NewRequest("GET", frontend.URL, nil)
		assert.Nil(t, err)
		req.Header.Set(api.AuthorizationHeaderKey, authToken)
		resp, err := http.DefaultClient.Do(req)

		// resp, err := http.Get(frontend.URL)

		assert.Nil(t, err)
		assert.Equal(t, 200, resp.StatusCode)
		assert.Equal(t, backendResponse, getBody(resp, t))
	})

	t.Run("400-bad-path", func(t *testing.T) {
		// test bad path
		frontend.URL = baseFrontURL + "/foo"
		resp, err := http.Get(frontend.URL)
		assert.Nil(t, err)
		assert.Equal(t, 400, resp.StatusCode)
		assert.Equal(t, getBody(resp, t), `{"kind":"Status","apiVersion":"v1","metadata":{},"status":"Failure","message":"bad path","reason":"BadRequest","code":400}`+"\n")
	})

	defer frontend.Close()
}
