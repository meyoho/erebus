package request

import (
	"k8s.io/apimachinery/pkg/util/sets"
	"k8s.io/apiserver/pkg/endpoints/request"
)

//NewRequestInfoResolver create a new RequestInfoFactory
func NewRequestInfoResolver() request.RequestInfoResolver {
	f := request.RequestInfoFactory{
		APIPrefixes:          sets.NewString("api", "apis"),
		GrouplessAPIPrefixes: sets.NewString("api"),
	}
	return &f
}
