package request

import (
	"net/http"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/apiserver/pkg/endpoints/handlers"
	"k8s.io/apiserver/pkg/endpoints/handlers/responsewriters"
)

//NewDefaultRequestScope create a default RequestScope
//Since we don't actually care about gvk in the proxy, this can be used for all request
func NewDefaultRequestScope() *handlers.RequestScope {
	scheme := runtime.NewScheme()
	negotiatedSerializer := serializer.NewCodecFactory(scheme).WithoutConversion()
	scope := handlers.RequestScope{
		Serializer: negotiatedSerializer,
	}
	return &scope
}

//WriteError is a thin wrapper for write proxy errors to http response
func WriteError(scope *handlers.RequestScope, err error, w http.ResponseWriter, req *http.Request) {
	responsewriters.ErrorNegotiated(err, scope.Serializer, scope.Kind.GroupVersion(), w, req)
}
