package api

const (
	// ProxyPathPrefix is the prefix for every request incoming to this proxy
	ProxyPathPrefix = "kubernetes"

	//ServerComponent is used for metrics recording
	ServerComponent string = "erebus"
)

const (

	//ProxyPath is a key used in request context to store the origin proxy path to be recorded in audit
	ProxyPath = "proxy-path"

	//ProxyOriginToken keep the origin auth token in context. Since we want to add audit ,and we want to
	// record user info, the default middleware will remove the auth token. In order to proxy the token to the
	// backend kubernetes clusters, we have to keep this token
	ProxyOriginToken = "proxy-origin-token"

	//AuthorizationHeaderKey is just a const
	AuthorizationHeaderKey = "Authorization"
)
